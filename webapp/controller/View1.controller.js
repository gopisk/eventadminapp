sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	'sap/ui/core/BusyIndicator',
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox"
], function (Controller, Filter, FilterOperator, BusyIndicator, JSONModel, MessageBox) {
	"use strict";

	return Controller.extend("com.capgemini.EventAdmin.controller.View1", {
		onInit: function () {

			var oViewModel = new JSONModel({
				worklistTableTitle: this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("worklistTableTitle"),
				attachmentTitle: this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("attachmentTitle"),
				ideaTitle: this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("ideaTitle")

			});
			this.getView().setModel(oViewModel, "worklistView");
			this.aFilters = [];
			var countriesPath = jQuery.sap.getModulePath("com.capgemini.EventAdmin.model", "/countries.json");
			this.oCountriesOfflineModel = new JSONModel();
			this.oCountriesOfflineModel.loadData(countriesPath, "", false);
			this.getView().setModel(this.oCountriesOfflineModel, "oCountriesOfflineModel");
			this.countriesData = this.getView().getModel("oCountriesOfflineModel").getData();

		},
		onLiveCountReceived: function (oEvent) {

			var liveCount = oEvent.getParameter("data").results.length;

			var sTitle = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("worklistTableTitleCount", [liveCount]);

			//	sTitle = this.getResourceBundle().getText("worklistTableTitle");

			this.getView().getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
		},
		onAttachmentCountReceived: function (oEvent) {

			var liveCount = oEvent.getParameter("data").results.length;

			var sTitle = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("attachmentCount", [liveCount]);

			//	sTitle = this.getResourceBundle().getText("worklistTableTitle");

			this.getView().getModel("worklistView").setProperty("/attachmentTitle", sTitle);
		},
		onIdeaCountReceived: function (oEvent) {

			var liveCount = oEvent.getParameter("data").results.length;

			var sTitle = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("ideaCount", [liveCount]);

			//	sTitle = this.getResourceBundle().getText("worklistTableTitle");

			this.getView().getModel("worklistView").setProperty("/ideaTitle", sTitle);
		},
		onTagFilterNew: function (oEvent) {
			debugger;
			if (this.aFilters.length !== 0) {
				debugger;
				if (this.aFilters[0].sPath === "name") {
					for (var i = 0; i = this.aFilters.length; i++) {
						this.aFilters.pop();
					}
				}
			}

			var selectedItem = oEvent.getParameter("changedItem"),
				bSelected = oEvent.getParameter("selected"),
				list = this.byId("idRegistrationTable").getBinding("items"), //getBinding("items"),

				that = this;

			function arrayRemove(arr, value) {
				return arr.filter(function (ele) {
					return ele.oValue1 !== value;
				});
			}

			if (bSelected) {
				// add to the filter array
				var f = new Filter({
					path: "Country",
					operator: "EQ",
					value1: selectedItem.getProperty("text")

				});

				var f1 = new Filter({
					path: "Country",
					operator: "EQ",
					value1: ""

				});
				this.aFilters.push(f);
				this.aFilters.push(f1);
			} else {
				// remove from the filter array
				this.aFilters = arrayRemove(this.aFilters, selectedItem.getProperty("text"));

			}

			this.getView().getModel().read("/USERDETAILS", {
				filters: this.aFilters,
				success: function (oData, response) {
					debugger;

					//	if (oData.results.length !== 0) {
					var aCaseFilters = that._createFilters(oData.results);
					list.filter(aCaseFilters);
					//	}

				}.bind(this),
				error: function (oError) {

				}
			});

		},
		_createFilters: function (results) {
			debugger;
			var aF = [];
			for (var i = 0; i < results.length; i++) {
				var value = results[i].Email;
				var f = new Filter({
					path: "Email",
					operator: "EQ",
					value1: value

				});
				aF.push(f);
			}
			return aF;
		},

		onAttachmentFilterNew: function (oEvent) {
			debugger;
			if (this.aFilters.length !== 0) {
				debugger;
				if (this.aFilters[0].sPath === "name") {
					for (var i = 0; i = this.aFilters.length; i++) {
						this.aFilters.pop();
					}
				}
			}

			var attachmentselectedItem = oEvent.getParameter("changedItem"),
				attachmentbSelected = oEvent.getParameter("selected"),
				attachmentlist = this.getView().byId("idAttachmentTable").getBinding("items"), //getBinding("items"),

				that = this;

			function arrayRemove(arr, value) {
				return arr.filter(function (ele) {
					return ele.oValue1 !== value;
				});
			}

			if (attachmentbSelected) {
				// add to the filter array
				var attachmentf = new Filter({
					path: "OwnerCountry",
					operator: "EQ",
					value1: attachmentselectedItem.getProperty("text")

				});

				var attachtmentf1 = new Filter({
					path: "OwnerCountry",
					operator: "EQ",
					value1: ""

				});
				this.aFilters.push(attachmentf);
				this.aFilters.push(attachtmentf1);
			} else {
				// remove from the filter array
				this.aFilters = arrayRemove(this.aFilters, attachmentselectedItem.getProperty("text"));

			}

			this.getView().getModel().read("/CV_EventCollateral", {
				filters: this.aFilters,
				success: function (oData, response) {
					var aCaseFilters = that._createAttachmentFilters(oData.results);
					attachmentlist.filter(aCaseFilters);

				},
				error: function (oError) {

				}
			});

		},
		_createAttachmentFilters: function (results) {
			debugger;
			var aF = [];
			for (var i = 0; i < results.length; i++) {
				var value = results[i].OwnerEmail;
				var f = new Filter({
					path: "OwnerEmail",
					operator: "EQ",
					value1: value

				});
				aF.push(f);
			}
			return aF;
		},

		onIdeaFilterNew: function (oEvent) {
			debugger;
			if (this.aFilters.length !== 0) {
				debugger;
				if (this.aFilters[0].sPath === "name") {
					for (var i = 0; i = this.aFilters.length; i++) {
						this.aFilters.pop();
					}
				}
			}

			var ideaselectedItem = oEvent.getParameter("changedItem"),
				ideabSelected = oEvent.getParameter("selected"),
				idealist = this.getView().byId("idIdeaTable").getBinding("items"), //getBinding("items"),

				that = this;

			function arrayRemove(arr, value) {
				return arr.filter(function (ele) {
					return ele.oValue1 !== value;
				});
			}

			if (ideabSelected) {
				// add to the filter array
				var ideaf = new Filter({
					path: "OwnerCountry",
					operator: "EQ",
					value1: ideaselectedItem.getProperty("text")

				});

				var ideaf1 = new Filter({
					path: "OwnerCountry",
					operator: "EQ",
					value1: ""

				});
				this.aFilters.push(ideaf);
				this.aFilters.push(ideaf1);
			} else {
				// remove from the filter array
				this.aFilters = arrayRemove(this.aFilters, ideaselectedItem.getProperty("text"));

			}

			this.getView().getModel().read("/IDEASUBMISSION", {
				filters: this.aFilters,
				success: function (oData, response) {
					var aCaseFilters = that._createIdeaFilters(oData.results);
					idealist.filter(aCaseFilters);

				},
				error: function (oError) {

				}
			});

		},
		_createIdeaFilters: function (results) {
			debugger;
			var aF = [];
			for (var i = 0; i < results.length; i++) {
				var value = results[i].OwnerEmail;
				var f = new Filter({
					path: "OwnerEmail",
					operator: "EQ",
					value1: value

				});
				aF.push(f);
			}
			return aF;
		},
		onDownloadPress: function (oEvent) {
			debugger;
			var fileName = oEvent.getSource().getBindingContext().getProperty("OwnerEmail");
			var that = this;
			sap.ui.core.BusyIndicator.show();
			var oModel = this.getView().getModel();
			oModel.read("/ATTACHMENT('" + fileName + "')", {
				method: "GET",
				success: function (data, response) {
					debugger;
					sap.ui.core.BusyIndicator.hide();
					var fName = data.Filename;
					var fType = data.Filetype;
					var fContent = data.Filecontent;

					var c = fContent.replace(/(\r\n|\n|\r)/gm, "");
					var file = new File([base64ToArrayBuffer(fContent)], fName, {
						type: fType
					});

					function base64ToArrayBuffer(base64) {
						var binary_string = window.atob(base64);
						var len = binary_string.length;
						var bytes = new Uint8Array(len);
						for (var i = 0; i < len; i++) {
							bytes[i] = binary_string.charCodeAt(i);
						}
						return bytes.buffer;
					}
					var fileURL = URL.createObjectURL(file);
					var a = document.createElement('a');
					a.download = fName;
					a.href = fileURL;
					a.click();

				},
				error: function (e) {
					debugger;
					sap.ui.core.BusyIndicator.hide();
					//	console.log(e)
					alert("error");
				}
			})
		},
		onRefreshPress: function () {
			debugger;
			this.getView().getModel().refresh();
			sap.ui.core.BusyIndicator.show();

			jQuery.sap.delayedCall(1000, this, function () {
				sap.ui.core.BusyIndicator.hide();
			});
		},
		base64ToArrayBuffer: function (base64) {
			var binary_string = window.atob(base64);
			var len = binary_string.length;
			var bytes = new Uint8Array(len);
			for (var i = 0; i < len; i++) {
				bytes[i] = binary_string.charCodeAt(i);
			}
			return bytes.buffer;
		},
		onDownloadSelectedPress: function () {
			debugger;
			var selectedCount = this.getView().byId("idAttachmentTable").getSelectedItems().length;
			if (selectedCount !== 0) {
				for (var d = 0; d < selectedCount; d++) {
					var selectedMail = this.getView().byId("idAttachmentTable").getSelectedItems()[0].getBindingContext().getProperty("OwnerEmail");
					this.DownloadAllfile(selectedMail);
				}
			}
			if (selectedCount === 0) {
				sap.m.MessageBox.information("Please select any to download");
			}
		},
		onDownloadAllPress: function () {
			debugger;
			var that = this;
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			sap.m.MessageBox.confirm(
				"Please make sure to download all the file?", {
					actions: [sap.m.MessageBox.Action.OK, sap.m.MessageBox.Action.CANCEL],
					styleClass: bCompact ? "sapUiSizeCompact" : "",
					onClose: function (sAction) {
						if (sAction === "OK") {

							var oDataModel = that.getView().getModel();
							sap.ui.core.BusyIndicator.show();
							oDataModel.read("/ATTACHMENT", {
								success: function (oData, oResponse) {
									debugger;

									for (var i = 0; i < oData.results.length; i++) {
										that.DownloadAllfile(oData.results[i].TeamLeadMail);

									}

								}.bind(that),
								error: function (error) {

								}
							});
						}
					}
				}
			);

		},
		DownloadAllfile: function (TeamLeadMail) {
			debugger;
			var fileName = TeamLeadMail
			var that = this;
			sap.ui.core.BusyIndicator.show();
			var oModel = this.getView().getModel();
			oModel.read("/ATTACHMENT('" + fileName + "')", {
				method: "GET",
				success: function (data, response) {
					debugger;

					var fName = data.Filename;
					var fType = data.Filetype;
					var fContent = data.Filecontent;

					var c = fContent.replace(/(\r\n|\n|\r)/gm, "");
					var file = new File([base64ToArrayBuffer(fContent)], fName, {
						type: fType
					});

					function base64ToArrayBuffer(base64) {
						var binary_string = window.atob(base64);
						var len = binary_string.length;
						var bytes = new Uint8Array(len);
						for (var i = 0; i < len; i++) {
							bytes[i] = binary_string.charCodeAt(i);
						}
						return bytes.buffer;
					}
					var fileURL = URL.createObjectURL(file);
					var a = document.createElement('a');
					a.download = fName;
					a.href = fileURL;
					a.click();
					sap.ui.core.BusyIndicator.hide();

				},
				error: function (e) {
					debugger;
					sap.ui.core.BusyIndicator.hide();
					//	console.log(e)
					alert("error");
				}
			})

		},

		onSearchRegistration: function (oEvent) {
			debugger;
			var sQuery = oEvent.getSource().getValue();
			if (sQuery && sQuery.length > 0) {
				var filter = [];
				var searchFilter = new Filter([
					/*new Filter("TeamLeadMail", "Contains", sQuery)*/
					this.createFilter("Email", FilterOperator.Contains, sQuery, true),
					this.createFilter("FirstName", FilterOperator.Contains, sQuery, true),
					this.createFilter("LastName", FilterOperator.Contains, sQuery, true),
					this.createFilter("Country", FilterOperator.Contains, sQuery, true)

				], false);
				filter.push(searchFilter);
				var oTableLiveSearch = this.byId("idRegistrationTable");
				oTableLiveSearch.getBinding("items").filter(searchFilter, "Application");

			}
			if (sQuery === "") {
				var filterReturn = [];

				filterReturn.push();
				var oTableLiveSearchReturn = this.byId("idRegistrationTable");
				oTableLiveSearchReturn.getBinding("items").filter(filterReturn, "Application");

			}
		},

		onAttachmentSearch: function (oEvent) {
			debugger;
			var sQuery = oEvent.getSource().getValue();
			if (sQuery && sQuery.length > 0) {
				var filter = [];
				var searchFilter = new Filter([
					/*new Filter("TeamLeadMail", "Contains", sQuery)*/
					this.createFilter("OwnerEmail", FilterOperator.Contains, sQuery, true),
					this.createFilter("OwnerCountry", FilterOperator.Contains, sQuery, true)

				], false);
				filter.push(searchFilter);
				var oTableLiveSearch = this.byId("idAttachmentTable");
				oTableLiveSearch.getBinding("items").filter(searchFilter, "Application");

			}
			if (sQuery === "") {
				var filterReturn = [];

				filterReturn.push();
				var oTableLiveSearchReturn = this.byId("idAttachmentTable");
				oTableLiveSearchReturn.getBinding("items").filter(filterReturn, "Application");

			}
		},
		onIdeaSearch: function (oEvent) {
			debugger;
			var sQuery = oEvent.getSource().getValue();
			if (sQuery && sQuery.length > 0) {
				var filter = [];
				var searchFilter = new Filter([
					/*new Filter("TeamLeadMail", "Contains", sQuery)*/
					this.createFilter("OwnerEmail", FilterOperator.Contains, sQuery, true),
					this.createFilter("TeamName", FilterOperator.Contains, sQuery, true),
					this.createFilter("OwnerCountry", FilterOperator.Contains, sQuery, true)

				], false);
				filter.push(searchFilter);
				var oTableLiveSearch = this.byId("idIdeaTable");
				oTableLiveSearch.getBinding("items").filter(searchFilter, "Application");

			}
			if (sQuery === "") {
				var filterReturn = [];

				filterReturn.push();
				var oTableLiveSearchReturn = this.byId("idIdeaTable");
				oTableLiveSearchReturn.getBinding("items").filter(filterReturn, "Application");

			}
		},

		/*onSearch: function (oEvent) {
			debugger;
			var sQuery = oEvent.getSource().getValue();
			if (sQuery && sQuery.length > 0) {
				var filter = [];
				var searchFilter = new Filter([
				
					this.createFilter("TeamLeadMail", FilterOperator.Contains, sQuery, true)

				], false);
				filter.push(searchFilter);
				var oTableLiveSearch = this.byId("idOwnerUserTable");
				oTableLiveSearch.getBinding("items").filter(searchFilter, "Application");

			}
			if (sQuery === "") {
				var filterReturn = [];

				filterReturn.push();
				var oTableLiveSearchReturn = this.byId("idOwnerUserTable");
				oTableLiveSearchReturn.getBinding("items").filter(filterReturn, "Application");

			}
		},*/

		createFilter: function (key, operator, value, useToLower) {

			return new Filter(useToLower ? "tolower(" + key + ")" : key, operator, useToLower ? "'" + value.toLowerCase() + "'" : value);
		}
	});
});